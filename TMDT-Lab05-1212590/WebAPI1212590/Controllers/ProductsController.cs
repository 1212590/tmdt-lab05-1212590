﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI1212590.Models;

namespace WebAPI1212590.Controllers
{
    public class ProductsController : ApiController
    {
        static readonly IProductRepository repository = new ProductRepository();

        public IEnumerable<Product> GetProductsByCategory1212590(string category)
        {
            return repository.GetAllProduct1212590().Where(
                p => string.Equals(p.Category, category, StringComparison.OrdinalIgnoreCase));
        }
    }
}
