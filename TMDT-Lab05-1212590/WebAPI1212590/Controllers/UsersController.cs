﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI1212590.Models;

namespace WebAPI1212590.Controllers
{
    public class UsersController : ApiController
    {
        static readonly IUserRepository repository = new UserRepository();

        public IEnumerable<User> GetAllUsers1212590()
        {
            return repository.GetAll1212590();
        }

        public User GetUser1212590(string username)
        {
            User user = repository.Get1212590(username);
            if (user == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return user;
        }

        [Authorize]
        public HttpResponseMessage PostUser1212590(User user)
        {
            user = repository.Add1212590(user);
            var response = Request.CreateResponse<User>(HttpStatusCode.Created, user);

            string uri = Url.Link("DefaultApi", new { username = user.UserName });
            response.Headers.Location = new Uri(uri);
            return response;
        }

        [Authorize]
        public void PutUser1212590(string username, User user)
        {
            user.UserName = username;
            if (!repository.Update1212590(user))
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        [Authorize]
        public void DeleteUser1212590(string username)
        {
            User user = repository.Get1212590(username);
            if (user == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            repository.Remove1212590(username);
        }
    }
}
