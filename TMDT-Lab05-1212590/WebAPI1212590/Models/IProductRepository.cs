﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI1212590.Models
{
    public interface IProductRepository
    {
        IEnumerable<Product> GetAllProduct1212590();
        Product AddProduct1212590(Product item);
    }
}
