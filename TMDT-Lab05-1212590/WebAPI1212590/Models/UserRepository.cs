﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI1212590.Models
{
    public class UserRepository : IUserRepository
    {
        private List<User> users = new List<User>();

        public UserRepository()
        {
            Add1212590(new User { UserName = "1212590", Password = "1212590", Name = "Le Khanh Son" });
            Add1212590(new User { UserName = "facebook", Password = "facebook", Name = "Facebook" });
            Add1212590(new User { UserName = "google", Password = "google", Name = "Google" });
            Add1212590(new User { UserName = "yahoo", Password = "yahoo", Name = "Yahoo" });
            Add1212590(new User { UserName = "wiki", Password = "wiki", Name = "Wiki" });
        }

        public IEnumerable<User> GetAll1212590()
        {
            return users;
        }

        public User Get1212590(string username)
        {
            return users.Find(u => u.UserName == username);
        }

        public User Add1212590(User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            users.Add(user);
            return user;
        }

        public void Remove1212590(string username)
        {
            users.RemoveAll(u => u.UserName == username);
        }

        public bool Update1212590(User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            int index = users.FindIndex(u => u.UserName == user.UserName);
            if (index == -1)
            {
                return false;
            }            
            users.RemoveAt(index);
            users.Add(user);
            return true;
        }
    }
}