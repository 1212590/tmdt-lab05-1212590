﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI1212590.Models
{
    public class User
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
    }
}