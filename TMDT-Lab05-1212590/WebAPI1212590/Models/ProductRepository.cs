﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI1212590.Models
{
    public class ProductRepository : IProductRepository
    {
        private List<Product> products = new List<Product>();
        private int _nextId = 1;

        public ProductRepository()
        {
            AddProduct1212590(new Product { Name = "Tomato soup", Category = "Groceries", Price = 1.39M });
            AddProduct1212590(new Product { Name = "Yo-yo", Category = "Toys", Price = 3.75M });
            AddProduct1212590(new Product { Name = "Hammer", Category = "Hardware", Price = 16.99M });
        }

        public IEnumerable<Product> GetAllProduct1212590()
        {
            return products;
        }

        public Product AddProduct1212590(Product item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }
            item.Id = _nextId++;
            products.Add(item);
            return item;
        }
    }
}