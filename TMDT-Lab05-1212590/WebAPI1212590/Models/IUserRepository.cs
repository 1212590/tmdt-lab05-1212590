﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI1212590.Models
{
    interface IUserRepository
    {
        IEnumerable<User> GetAll1212590();
        User Get1212590(string username);
        User Add1212590(User user);
        void Remove1212590(string username);
        bool Update1212590(User user);
    }
}
